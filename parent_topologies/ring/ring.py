from mergexp import * 
# 
# Parameterized Ring topology generator
# This creates a ring of size n with little clusters hanging off specified nodes
net = Network('ring', routing == static, addressing == ipv4)
ring_size = 4
subnet_size = 2 
subnet_pos = [0,2]
# Construct the core ring
ring = [net.node('ring_node_{:02d}'.format(i)) for i in range(0, ring_size)]
for i in range(1, len(ring)):
    net.connect(ring[i - 1 : i + 1])
net.connect([ring[0], ring[-1]])
# Now do the subnet generation
subnets = [net.node('subnet_node_{:02d}'.format(i)) for i in range(0, subnet_size * len(subnet_pos))]
for index,value  in enumerate(subnet_pos):
    for j in range(0, subnet_size):
        net.connect([ring[value]] +  subnets[(index * subnet_size):((index + 1)*subnet_size)])
# Construct endpoints
experiment(net)
