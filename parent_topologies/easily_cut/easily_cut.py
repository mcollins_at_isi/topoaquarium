# easily cut model
from mergexp import * 
net = Network('easilycut', routing == static, addressing == ipv4)
top_leg = [net.node('top_node_{:02d}'.format(i)) for i in range(0,3)]
bottom_leg = [net.node('bottom_node_{:02d}'.format(i)) for i in range(0,2)]
middle_leg = [net.node('middle_node_{:02d}'.format(i)) for i in range(0,3)]
for i in range(0, len(top_leg) -1 ):
    net.connect([top_leg[i], top_leg[i+1]])
for i in range(len(bottom_leg) -1 ):
    net.connect([bottom_leg[i], bottom_leg[i+1]])
for i in range(len(middle_leg) -1 ):
    net.connect([middle_leg[i], middle_leg[i+1]])
left_iap = net.node('left_iap')
right_iap = net.node('right_iap')
net.connect([left_iap, top_leg[0]])
net.connect([left_iap, middle_leg[0]])
net.connect([middle_leg[0], bottom_leg[0]])
net.connect([right_iap, top_leg[-1]])
net.connect([right_iap, middle_leg[-1]])
net.connect([middle_leg[-1], bottom_leg[-1]])
experiment(net)
