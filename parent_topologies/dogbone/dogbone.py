from mergexp import * 
net = Network('dogbone', routing == static, addressing == ipv4)
# Standard dogbone topology -- a spine of x hosts followed by two endpoitns
spine_len = 4
endpoint_len = 3
# Construct spine
spine = [net.node('spinal_node_{:02d}'.format(i)) for i in range(0, spine_len)]
for i in range(1, len(spine)):
    net.connect(spine[i - 1 : i + 1])
# Construct endpoints

end_left = [net.node('el_node_{:02d}'.format(i)) for i in range(0, endpoint_len)]
end_right = [net.node('er_node_{:02d}'.format(i)) for i in range(0, endpoint_len)]
for i in end_left: 
    net.connect([i, spine[0]])
for i in end_right: 
    net.connect([i, spine[-1]])
experiment(net)

