import itertools
from mergexp import * 
# 
# Parameterized 'ring and core' topology
# This topology represents a more complex enterprise network with an outer ring, an inner core mesh
# and a set of sub-elements 
net = Network('ring', routing == static, addressing == ipv4)
outer_ring_size = 6 
inner_mesh_size = 6
office_size = 2
# Create all the nodes
outer_ring_nodes = [net.node('ring_{:02d}'.format(i)) for i in range(outer_ring_size)]
inner_mesh_nodes = [net.node('mesh_{:02d}'.format(i)) for i in range(inner_mesh_size)]
office_nodes = {'subnet_node_{:02d}.{:02d}'.format(i,j): net.node('subnet_node_{:02d}.{:02d}'.format(i,j)) for i,j in itertools.product(range(inner_mesh_size), range(office_size))}
# Construct the outer ring
for i in range(outer_ring_size - 1):
    net.connect([outer_ring_nodes[i], outer_ring_nodes[i+1]])
net.connect([outer_ring_nodes[0], outer_ring_nodes[-1]])
net.connect(inner_mesh_nodes)
for i,j in itertools.product(range(inner_mesh_size), range(office_size)):
    elt = office_nodes['subnet_node_{:02d}.{:02d}'.format(i,j)]
    net.connect([elt, inner_mesh_nodes[i]])
# Link inner and outer
for i in range(inner_mesh_size):
    net.connect([inner_mesh_nodes[i], outer_ring_nodes[i]])
experiment(net)
